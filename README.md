# tomcat8-openjdk8-alpine

Guia basica para iniciar una aplicacion web en Tomcat con Docker

* Instalar [Docker](https://docs.docker.com/install/).
* Clonar este repositorio - $git clone https://gitlab.com/CristianPastrana/tomcat8-openjdk8-alpine.git
* cd tomcat8-openjdk8-alpine
* $docker build -t test_app .
* $docker run -p --name container_name 80:8080 test_app
* http://localhost:80



